white_list_etc=(
  'apparmor.d/*'
  'default/grub'
  '.config/*'
  '.kde4/share/config/*'
  '.zsh/*'
)

IgnorePath '/boot/*'
# IgnorePathsExcept '/etc' "${white_list_etc[@]}"
IgnorePath '/etc/*'
IgnorePath '/opt/*'
IgnorePath '/swap'
IgnorePath '/usr/*'
IgnorePath '/var/*'
IgnorePath '/*.ko'

# Thu Apr 25 08:24:49 PM CEST 2024 - Unknown packages
AddPackage alacritty
AddPackage android-tools # adb
AddPackage ascii
AddPackage aspell-en
AddPackage base
AddPackage base-devel
AddPackage bat
AddPackage battop-bin
AddPackage binutils
AddPackage blueberry
AddPackage breeze-gtk
AddPackage breeze5
AddPackage ccache
AddPackage chromium
AddPackage cmake
AddPackage clang
AddPackage conan
AddPackage dunst
AddPackage dbus-broker
AddPackage dex
AddPackage dhcp
AddPackage dhcpcd
AddPackage dmenu
AddPackage dosfstools # mkfs.fat
AddPackage evince
AddPackage evolution
AddPackage exfat-utils
AddPackage eza
AddPackage fd
AddPackage firefox-developer-edition
AddPackage fish
AddPackage fuse2
AddPackage fzf # for LazyVim symbol search
AddPackage gammastep
AddPackage gcc
AddPackage gdb
AddPackage geogebra-6-bin
AddPackage git
AddPackage gnu-netcat
AddPackage gnuplot # for qalc
AddPackage grim
AddPackage groff
AddPackage grub
AddPackage glib2-devel # there's some package in here that's missing in gimp
AddPackage gopls       # go language server
AddPackage hollywood
AddPackage helix
AddPackage hexedit
AddPackage htop
AddPackage imagemagick
AddPackage img2pdf
AddPackage imhex-bin
AddPackage imv
AddPackage inkscape
AddPackage inotify-tools
AddPackage intel-ucode
AddPackage jdk-openjdk
AddPackage jupyter-notebook
AddPackage jq
AddPackage kdeconnect
AddPackage keepassxc
AddPackage kinfocenter
AddPackage lib32-fontconfig # counter strike: source
AddPackage lib32-mesa
AddPackage lib32-openal
AddPackage libbsd
AddPackage libc++
AddPackage libreoffice-fresh
AddPackage libunwind
AddPackage linux
AddPackage linux-firmware
AddPackage linux-headers
AddPackage lld
AddPackage lldb
AddPackage llvm
AddPackage make
AddPackage man-db
AddPackage mesa-utils
AddPackage meson
AddPackage mold
AddPackage monero-gui
AddPackage neovim
AddPackage networkmanager
AddPackage network-manager-applet
AddPackage nmap
AddPackage noto-fonts-emoji
AddPackage ntfs-3g
AddPackage ntp
AddPackage obs-studio
AddPackage otf-font-awesome
AddPackage pandoc-cli
AddPackage payload-dumper-go # android image extraction
AddPackage p7zip
AddPackage pasystray
AddPackage patch
AddPackage pavucontrol
AddPackage perf
AddPackage pipewire-pulse
AddPackage pkgconf
AddPackage plasma-desktop
AddPackage plymouth
AddPackage prometheus
AddPackage python-ipykernel
AddPackage python-matplotlib
AddPackage python-opencv
AddPackage python-pip
AddPackage python-scipy
AddPackage qrencode
AddPackage qt5ct
AddPackage refind
AddPackage ripgrep
AddPackage rustic
AddPackage rsync
AddPackage sddm
AddPackage slurp
AddPackage sioyek-appimage
AddPackage steam
AddPackage strace
AddPackage sudo
AddPackage sway
AddPackage swaylock
AddPackage sweethome3d
AddPackage sx
AddPackage syncthing
AddPackage tagspaces-bin
AddPackage telegram-desktop
AddPackage thunderbird
AddPackage tk
AddPackage torbrowser-launcher
AddPackage tesseract-data-eng # ocr
AddPackage tinymist           # typst language server
AddPackage translate-shell
AddPackage tree
AddPackage typst
AddPackage udisks2
AddPackage ufw
AddPackage unrar
AddPackage unzip
AddPackage vim
AddPackage vimiv
AddPackage vlc
AddPackage w3m
AddPackage waybar
AddPackage which
AddPackage whois
AddPackage wl-clipboard
AddPackage wmctrl
AddPackage xclip
AddPackage xdg-desktop-portal-wlr
AddPackage xournalpp
AddPackage xorg-server
AddPackage xorg-xev
AddPackage xorg-xhost
AddPackage xorg-xinit
AddPackage xorg-xkbprint
AddPackage xorg-xwayland
AddPackage xorg-xwininfo
AddPackage xterm # startx
AddPackage yazi
AddPackage yq
AddPackage yt-dlp
AddPackage zip

# Thu Apr 25 08:24:53 PM CEST 2024 - Unknown foreign packages

AddPackage --foreign anki-bin
AddPackage --foreign aconfmgr-git
AddPackage --foreign bazelisk
AddPackage --foreign gtk-theme-material-black
AddPackage --foreign kickoff
AddPackage --foreign kmonad-bin
AddPackage --foreign ocrmypdf
AddPackage --foreign python-frida-tools
AddPackage --foreign python39
AddPackage --foreign steamcmd
AddPackage --foreign swayidle
AddPackage --foreign tailwindcss
AddPackage --foreign ueberzugpp
AddPackage --foreign update-grub
AddPackage --foreign vscodium-bin
AddPackage --foreign xtrace
AddPackage --foreign yay

if [[ "${HOSTNAME}" == "archlinux" ]]; then
  AddPackageGroup gnome
  AddPackage foot
  AddPackage brightnessctl
  AddPackage efibootmgr
  AddPackage foot
  AddPackage fwupd
  AddPackage intel-media-driver
  AddPackage iwd # networking
  AddPackage libva-intel-driver
  AddPackage libva-mesa-driver
  AddPackage vulkan-intel
  AddPackage vulkan-radeon
  AddPackage wireless_tools
  AddPackage xf86-video-amdgpu
  AddPackage xf86-video-ati
  AddPackage xf86-video-nouveau
  AddPackage xf86-video-vmware
else
  AddPackage i3-wm
  AddPackage i3lock
  AddPackage i3status
fi

# Thu Apr 25 08:56:15 PM CEST 2024 - New / changed files

# CopyFile /etc/apparmor.d/default 600
# CopyFile /etc/apparmor.d/usr.bin.deno 600
# CopyFile /etc/default/grub
